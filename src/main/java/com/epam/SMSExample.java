package com.epam;

import com.twilio.Twilio;
import com.twilio.rest.api.v2010.account.Message;
import com.twilio.type.PhoneNumber;

public class SMSExample {
    public static final String ACCOUNT_SID =
            "AC97bed659d86c1193bd2225a303a34f26";
    public static final String AUTH_TOKEN =
            "cb936762a1f71dc58c32029bf7c40889";

    public static void sendMessage(String str) {
        Twilio.init(ACCOUNT_SID, AUTH_TOKEN);
        Message message = Message
                .creator(new PhoneNumber("+380938655163"),
                        new PhoneNumber("+13049312056"), str)
                .create();
    }
}
